//
//  ContentView.swift
//  My Calculator
//
//  Created by M Ramdhan Syahputra on 06/02/24.
//

import SwiftUI

struct ContentView: View {
    let padNumbers: [[PadNumber]] = [
        [.clear, .minusOrPlus, .percent, .divide],
        [.seven, .eight, .nine, .multiply],
        [.four, .five, .six, .substract],
        [.one, .two, .three, .plus],
        [.zero, .decimal, .equal],
    ]
    
    var body: some View {
        ZStack {
            Color.black.ignoresSafeArea()
            
            VStack {
                Spacer()
                
                HStack {
                    Spacer()
                    Text("0")
                        .foregroundStyle(.white)
                        .font(.largeTitle.bold())
                        .padding()
                }
                
                ForEach(padNumbers, id: \.self) { row in
                    HStack(spacing: 12) {
                        ForEach(row, id: \.self) { value in
                            Text(value.rawValue)
                                .font(.largeTitle)
                                .foregroundStyle(value.numberColor)
                                .frame(
                                    width: value == .zero ? 170 : 80,
                                    height: 80
                                )
                                .background(value.padColor)
                                .clipShape(.rect(cornerRadius: 50))
                        }
                    }.padding(5)
                }
            }
        }
    }
}

enum PadNumber : String {
    case zero = "0"
    case one = "1"
    case two = "2"
    case three = "3"
    case four = "4"
    case five = "5"
    case six = "6"
    case seven = "7"
    case eight = "8"
    case nine = "9"
    case clear = "a/c"
    case plus = "+"
    case substract = "-"
    case divide = "/"
    case percent = "%"
    case multiply = "*"
    case minusOrPlus = "+/-"
    case decimal = ","
    case equal = "="
    
    var padColor: Color {
        switch self {
        case .clear, .minusOrPlus, .percent:
            return Color(.lightGray)
        case .divide, .multiply, .substract, .plus, .equal:
            return .orange
        default:
            return .secondary
        }
    }
    
    var numberColor: Color {
        switch self {
        case .clear, .minusOrPlus, .percent:
            return .black
        default:
            return .white
        }
    }
}

#Preview {
    ContentView()
}
