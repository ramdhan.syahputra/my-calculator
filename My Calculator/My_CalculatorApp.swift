//
//  My_CalculatorApp.swift
//  My Calculator
//
//  Created by M Ramdhan Syahputra on 06/02/24.
//

import SwiftUI

@main
struct My_CalculatorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
